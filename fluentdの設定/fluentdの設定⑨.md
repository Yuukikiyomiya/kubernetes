# fluent.confファイルの作成
```
mkdir -p  ~/fluentd/configmap
```
```
cat >  ~/fluentd/configmap/fluent.conf << EOF
@include systemd-kubelet.conf
@include systemd-containerd.conf
@include kubernetes.conf
<match systemd.*>
  @type remote_syslog
  @id out_kube_remote_syslog
  host "#{ENV['SYSLOG_HOST']}"
  port "#{ENV['SYSLOG_PORT']}"
  severity info
  program fluentd_systemd
  hostname \${tag[1]}

  protocol "#{ENV['SYSLOG_PROTOCOL'] || 'tcp'}"
  tls "#{ENV['SYSLOG_TLS'] || 'false'}"
  ca_file "#{ENV['SYSLOG_CA_FILE'] || ''}"
  verify_mode "#{ENV['SYSLOG_VERIFY_MODE'] || ''}"
  packet_size 65535

  <buffer tag>
   @type file
   path /var/log/fluent/buffer/systemd.*.buf
  </buffer>
</match>

<filter kubernetes.**>
  @type kubernetes_metadata
</filter>

<filter kubernetes.**>
  @type record_transformer
  enable_ruby
  remove_keys kubernetes,docker

  <record>
    hostname \${ record.dig("kubernetes", "host") }
    namespace \${ record.dig("kubernetes", "namespace_name") }
    podname \${ record.dig("kubernetes", "pod_name") }
    containername \${ record.dig("kubernetes", "container_name") }
  </record>
</filter>

<match kubernetes.**>
  @type remote_syslog
  @id out_k8s_remote_syslog
  host "#{ENV['SYSLOG_HOST']}"
  port "#{ENV['SYSLOG_PORT']}"
  severity info
  program fluentd_k8s
  hostname \${hostname}_\${namespace}

  protocol "#{ENV['SYSLOG_PROTOCOL'] || 'tcp'}"
  tls "#{ENV['SYSLOG_TLS'] || 'false'}"
  ca_file "#{ENV['SYSLOG_CA_FILE'] || ''}"
  verify_mode "#{ENV['SYSLOG_VERIFY_MODE'] || ''}"
  packet_size 65535

  <buffer hostname,namespace>
    @type file
    path /var/log/fluent/buffer/kubernetes.*.buf
  </buffer>

</match>
EOF
```

# systemd-kubelet.confファイルの作成
```
cat > ~/fluentd/configmap/systemd-kubelet.conf << EOF
<source>
  @type systemd
  @id in_systemd_kubelet
  matches [{ "_SYSTEMD_UNIT": "kubelet.service" }]
  <storage>
    @type local
    persistent true
    path /var/log/fluent/kubelet-cursor.json
  </storage>
  read_from_head true
  tag systemd.kubelet
</source>
EOF
```