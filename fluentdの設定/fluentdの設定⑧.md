# fluentd-logging ConfigMapの定義追記
```
cat > /tmp/fluent_volume_mount_append << EOF
        - name: config
          mountPath: /fluentd/etc
EOF
```
```
sed -i -e \
 '/mountPath: \/var\/log/r /tmp/fluent_volume_mount_append' \
 ~/fluentd-daemonset-syslog.yaml
```
```
cat > /tmp/fluent_volume_append << EOF
      - name: config
        configMap:
          name: fluentd-logging
EOF
```
```
sed -i -e \
 '/path: \/var\/log/r /tmp/fluent_volume_append' \
 ~/fluentd-daemonset-syslog.yaml
```

# tolerationsの設定追記
```
cat > /tmp/fluentd_tolerations << EOF
      - key: node-role.kubernetes.io/control-plane
        effect: NoSchedule
EOF
```
```
sed -i -e  '/NoSchedule/r /tmp/fluentd_tolerations' \
 ~/fluentd-daemonset-syslog.yaml
```