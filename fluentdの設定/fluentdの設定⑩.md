# systemd-containerd.confファイルの作成
```
cat > ~/fluentd/configmap/systemd-containerd.conf << EOF
<source>
  @type systemd
  @id in_systemd_containerd
  matches [{ "_SYSTEMD_UNIT": "containerd.service" }]
  <storage>
    @type local
    persistent true
    path /var/log/fluent/containerd-cursor.json
  </storage>
  read_from_head true
  tag systemd.containerd
</source>
EOF
```

# kubernetes.confファイルの作成
```
cat > ~/fluentd/configmap/kubernetes.conf << EOF
<source>
  @type tail
  @id in_tail_container_logs
  path /var/log/containers/*.log
  pos_file /var/log/fluent/fluentd-containers.log.pos
  tag kubernetes.*
  read_from_head true
  <parse>
    @type none
  </parse>
</source>
EOF
```