# kubernetes構築②
```
cat > ~/kubeadm-config.yaml << EOF
apiVersion: kubeadm.k8s.io/v1beta3 
kind: ClusterConfiguration
kubernetesVersion: v1.29.4
networking:
  podSubnet: 10.244.0.0/16 
apiServer:
  certSANs:
    - "cp.k8s.example.com" 
  extraArgs:
    audit-policy-file: /etc/kubernetes/audit/policy.yaml
    audit-log-path: /var/log/kubernetes/kube-apiserver-audit.log
    audit-log-maxage: "7" 
    audit-log-maxsize: "200" 
  extraVolumes: 
    - name: audit-policy
      hostPath: /etc/kubernetes/audit
      mountPath: /etc/kubernetes/audit
      pathType: DirectoryOrCreate
    - name: "audit-log"
      hostPath: /var/log/kubernetes
      mountPath: /var/log/kubernetes
      pathType: DirectoryOrCreate
controlPlaneEndpoint: "cp.k8s.example.com:6443"
---
kind: KubeletConfiguration
apiVersion: kubelet.config.k8s.io/v1beta1
cgroupDriver: systemd
EOF
```